var webpack = require('webpack');
var path = require('path');
var extractPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var autoprefixer = require('autoprefixer');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

let config = {
	entry: './src/js/main.js',
	devServer: {
		inline: true,
		contentBase: './build/dev',
		port: 3000,
        historyApiFallback: {
          index: 'index.html'
        }
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				loader: 'babel-loader',
				query: {
					presets: ['env','react']
				}
			},
			{
				test: /\.scss$/,
				use: extractPlugin.extract({
					fallback: 'style-loader',
					use: ['css-loader','postcss-loader','sass-loader']
				})
			},
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: '/fonts/'
                    }
                }]
            },
			{
				test: /\.html$/,
				use: ['html-loader']
			},
			{
				test: /\.(jpg|png|gif)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: './img/',
							publicPath: ''
						}
					}
				]
			}
		]
	},
	plugins: [
		new extractPlugin('style.css'),
		new webpack.DefinePlugin({
			'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
            }
		}),
		new HtmlWebpackPlugin({
            filename: 'index.html',
			template: './src/index.html'
		})
	]
}

if (process.env.NODE_ENV === 'production') {
    config.plugins.push(
		new UglifyJSPlugin()
	);
    config.output = {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'build/prod')
	};
}else {
    config.output = {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'build/dev')
	};
}

module.exports = config;