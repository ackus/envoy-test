# Coding Test Submission

### To run:
1. Download
2. In downloaded directory, run:
    * ```npm i```
    * ```npm run dev```
3. In your browser, visit:
>   localhost:3000

### Or visit view it online on my website:
* [projects.alexbretana.com/envoy](http://projects.alexbretana.com/envoy/)

### Some Notes:
* I abstracted page data into the main.js file to allow for possible loading of data from JSON via an API and also to make editing the page easier from one location
* I opted to _not_ use a state management library since this project is pretty small. I might have gone with MobX to handle the links populating the Nav since it's used in 2 locations.
* I added a 3rd slide with a shameless plug on it since 2 slides seemed pretty lonely
* I used some my own judgement on styling the tablet and mobile views of this page, but there are definitely things I would have liked to sit down and discuss with a designer to get their feelings on things (line breaks, padding, etc.)
* I spent more time than I wanted to staring at fonts and trying to find suitable alternatives, so forgive my font choice if it doesn't match up completely with the comp