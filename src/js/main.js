import React from 'react'
import ReactDOM from 'react-dom'

import MobileMenu from './components/mobileMenu'
import Header from './components/header'
import SimpleSlider from './components/slider'
import HomeBodySection from './components/homeBodySection'

require('../scss/style.scss');


//*****************************************************************************************************************************************
//************************************************************DATA AND SETTINGS************************************************************
//*****************************************************************************************************************************************
    //Page data that can be called externally from an API
const LinksForNav = [
    {
        title: 'Envoy',
        id: 'home-link',
        link: './'
    },
    {
        title: 'Work',
        id: '',
        link: './work'
    },
    {
        title: 'Services',
        id: '',
        link: './services'
    },
    {
        title: 'Ventures',
        id: '',
        link: './ventures'
    },
    {
        title: 'About',
        id: 'about-link',
        link: './about'
    },
    {
        title: 'Contact',
        id: 'contact-link',
        link: './contact'
    },
    {
        title: 'Blog',
        id: '',
        link: './blog'
    }
]
const SlideData = [
    {
        h3: "The iPhone<br />Unchained",
        p: "Tarantino's outrageous film Django Unchained needed an equally in your face mobile experience. Bloody good awesomeness ensued.",
        link: './',
        linkText: 'View Project',
        slideID: 'slide-iPhone'
    },
    {
        h3: "NETFLIX<br />&CRYSIS",
        p: "Just a few things you can do on a Windows PC. Can you do ALL of that on a Mac?!? Hmm??",
        link: './',
        linkText: 'View Project',
        slideID: 'slide-windows'
    },
    {
        h3: "Our Wedding<br />IX-XV-MMXVIII",
        p: "Only a few weeks to go! Are we ready? Logistically, no. Mentally and emotionall, yes!",
        link: 'http://www.lillyandalex.com',
        linkText: 'View Site',
        slideID: 'slide-wedding'
    }
]
const FeaturedWorkData = {
    h3: 'Featured Work',
    link: './work-link-here',
    linkText: 'View All Work',
    items: [
        {
            h5: 'Boost Mobile ~Rebrand~',
            p: 'Boost Mobile recognized the need to elevate and refresh its brand to speak to a broader audience. The result was an evolved logo, ...',
            link: './boost-mobile-link',
            linkText: 'View Project',
            thumb: './img/image1.jpg'
        },
        {
            h5: 'Pear Sports ~Training Portal~',
            p: 'Pear Sports revolutionary training systems and fitness apps were getting lost in a crowded marketplace. Envoy rebuilt the story and ...',
            link: './pear-sports-link',
            linkText: 'View Project',
            thumb: './img/image2.jpg'
        },
        {
            h5: '5.11 ~Ultimate Training Experience~',
            p: '5.11, the industry leader in tactical gear turned to Envoy to bring their "Ultimate Training Challenge" to life in the digital space ...',
            link: './511-link',
            linkText: 'View Project',
            thumb: './img/image3.jpg'
        }
    ]
}
const WireData = {
    h3: 'Wire',
    link: './news-link-here',
    linkText: 'View All News',
    items: [
        {
            h4: 'The Weinstein Co Lets Envoy Loose on Sin City 2',
            date: 'May 1, 2013',
            link: './news-link-1',
            linkText: 'Read More'
        },
        {
            h4: '5.11 Taps Envoy as New Digital Agency',
            date: 'May 1, 2013',
            link: './news-link-2',
            linkText: 'Read More'
        },
        {
            h4: '~Pear Breaks Ground on Ground Breaking App~',
            date: 'May 1, 2013',
            link: './news-link-3',
            linkText: 'Read More'
        }
    ]
}
const MobileMenuWidth = '300px';
    //banner slide settings
const SliderSettings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 4500
};
    //limit to number of items shown in homepage body collections
const UIItemLimit = 3;


//*******************************************************************************************************************************************
//************************************************************RENDER REACT COMPONENTS********************************************************
//*******************************************************************************************************************************************
    //could use MobX to share link data for the below 2 components, but this project is small enough right now
    //would consider using a state manangement library if the project got larger
ReactDOM.render(<MobileMenu width={MobileMenuWidth} links={LinksForNav}/>, document.getElementById('mobile-menu'));
ReactDOM.render(<Header links={LinksForNav}/>, document.querySelector('header'));
                
ReactDOM.render(<SimpleSlider slideData={SlideData} settings={SliderSettings}/>,document.getElementById('banner'));
ReactDOM.render(<HomeBodySection sectionData={FeaturedWorkData} limit={UIItemLimit}/>,document.getElementById('featured-work'));
ReactDOM.render(<HomeBodySection sectionData={WireData} limit={UIItemLimit} />,document.getElementById('the-wire'));

                
//*******************************************************************************************************************************************
//************************************************************EXTRA PAGE JS******************************************************************
//*******************************************************************************************************************************************
    //A little extra here to make the opaque nav bar more visible when scrolling past the banner
var bodyTop = document.getElementById('home-body').offsetTop;
var header = document.querySelector('header');
document.addEventListener('scroll',()=>{
    if(document.documentElement.scrollTop >= (bodyTop - 10)) {
        header.classList.add('no-opacity');
    }else {
        header.classList.remove('no-opacity');
    }
});