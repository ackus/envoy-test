import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter } from '@fortawesome/free-brands-svg-icons'

function TwitterBar() {
    return (
        <div id="twitter">
            <span className='icon'><FontAwesomeIcon icon={faTwitter} /></span><span className="tweet">Tweet. Tweet.</span><span className="plus">+</span>
        </div>
    );
}

export default TwitterBar;