import React from 'react'

function Slide(props) {
    return (
        <div id={props.id} key={props.key} className='banner-slide'>
            <div className='banner-slide-inner'>
                <h3>{props.h3}</h3>
                <p>{props.p} <a href={props.link} className="project-link">{props.linkText}</a></p>
            </div>
        </div>
    );
}

export default Slide