import React from 'react'
import HomeBodyCollectionItem from './homeBodyCollectionItem'

const reactStringReplace = require('react-string-replace')

class HomeBodySection extends React.Component {
    constructor(props) {
        super(props)
    }
    
    getItems() {
        let k = 0,
            items = this.props.sectionData.items.slice(0,this.props.limit);
        return items.map((x)=>(
            <HomeBodyCollectionItem {...x} key={k++}/>
        ))
    }
    
    render(){
        let items = this.getItems();
        return (
            <div className="home-body-section">
                <div id="featured-top" className="home-body-section-top">
                    <h3>{this.props.sectionData.h3}</h3>
                    <a href={this.props.sectionData.link}><em>{this.props.sectionData.linkText}</em></a>
                </div>
                <div id="featured-work-collection" className="home-body-section-collection">
                    {items}
                </div>
            </div>
        );
    }
}

export default HomeBodySection