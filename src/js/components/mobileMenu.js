import React from 'react'
import { slide as Menu } from 'react-burger-menu'

import Nav from './nav'

function MobileMenu(props) {
    return (
        <Menu isOpen={false} pageWrapId={ "main" } outerContainerId={ "menu-all-wrapper" } left width={props.width} >
            <Nav links={props.links} />
        </Menu>
    )
}

export default MobileMenu