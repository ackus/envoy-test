import React from 'react'

const reactStringReplace = require('react-string-replace')

function HomeBodyCollectionItem(props) {
    let thumb = (props.thumb) ? (
        <div className="collection-thumb">
            <img src={props.thumb} alt="" />
        </div>
    ): null;
    let H4 = null, k=1;
    
    if (props.h4) {
        H4 = reactStringReplace(props.h4,/~([^~]*)~/g, (match, i)=>(<em key={k++}>{match}</em>));
        H4 = (<h4>{H4}</h4>);
     }
    let H5 = null;
    if (props.h5) {
        H5 = reactStringReplace(props.h5,/~([^~]*)~/g, (match, i)=>(<em key={k++}>{match}</em>));
        H5 = (<h5>{H5}</h5>);
     }
                                                                    
    let p = (props.p) ? (<p>{props.p}</p>): null;
    let date = (props.date) ? (<span className='date'>{props.date}</span>): null;
                               
    return (
        <div className="collection-item">
            {thumb}
            {H4}{H5}
            {p}{date}
            <a href={props.link} className="collection-link">{props.linkText}</a>
        </div>
    );
}

export default HomeBodyCollectionItem