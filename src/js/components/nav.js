import React from 'react'

class Nav extends React.Component {
    constructor(props) {
        super(props)
    }
    
    getLinks() {
        let k = 0;
        return this.props.links.map((l)=>(
            <li key={k++}><a href={l.link} id={l.id}>{l.title}</a></li>
        ))
    }
    
    render(){
        return (
            <ul>
                {this.getLinks()}
            </ul>
        )
    }
}

export default Nav