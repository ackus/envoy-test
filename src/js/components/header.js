import React from 'react'

import Nav from './nav'
import Social from './social'

function Header(props) {
    return (
        <div className="deskInner">
            <nav>
                <Nav links={props.links}/>
            </nav>
            <Social />
        </div>
    )
}

export default Header