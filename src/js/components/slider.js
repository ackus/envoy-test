import React from 'react'
import Slider from 'react-slick'
import Slide from './slide'

const reactStringReplace = require('react-string-replace')

class SimpleSlider extends React.Component {
    constructor(props) {
        super(props)
    }
    
    getSlides() {
        let k = 1;
        
        return this.props.slideData.map( d => {
            let h3 = reactStringReplace(d['h3'],'<br />', (match, i) => (
                <br key={k++}/>
            ))
            h3 = reactStringReplace(h3,'&', (match, i) => (
                <span className='smaller' key={k++}>{match}</span>
            ))
            return (
                <Slide id={d['slideID']} key={d} h3={h3} p={d['p']} link={d['link']} linkText={d['linkText']} />
            )
        })
    }
    
    render() {
        let slides = this.getSlides();
        return (
            <Slider {...this.props.settings}>
                {slides}
            </Slider>
        );
    }
}

export default SimpleSlider;