import React from 'react'

import TwitterBar from './twitter'

function Social() {
    return (
        <div id="header-social">
            <TwitterBar />
        </div>
    );
}

export default Social;